<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 3.8.14
 * Time: 21:18
 */

class Category extends \Phalcon\Mvc\Model {

	public $id;
	public $id_name;
	public $id_parent;

	public function get($id = null){
		$categories =  $this::find("id=$id");
		foreach($categories as $category){
			$this->id = $category->id;
			$this->id_name = $category->id_name;
			$this->id_parent = $category->id_parent;
			return $this->toJson();
		}
	}
	public function post($request){
		return $this->save($this->request->getPost(), array('id_name','id_parent'));
	}
	public function delete($id){
		$category = $this::find("id=$id");
		return	$category->delete();
	}
	public function toJson(){
		return json_encode(array('id'=>$this->id,'id_name'=>$this->id_name,'id_parent'=>$this->id_parent));
	}
}