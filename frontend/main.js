/**
 * Created by Vojtěch Havel
 * Date: 8/3/14
 */

//var app = function (response) {
//
//
//    var addSeasons = function (seasons) {
//        //adds categories to document (to MainSelection)
//
//        for (var i = 0; i < seasons.length; i++) {
//
//            var myCategory = $("<div>");
//            myCategory.text(seasons[i].name);
//            myCategory.addClass("Category");
//            myLink = "summary/?id="+seasons[i].id;
//            $(".MainSelection").append(myCategory);
//            myCategory.wrap('<a style="display:inline-block" href='+ myLink+'></a>');
//
//        }        ;
//
//    }
//
//
//    console.log('response Text:\n' + response.responseText + '\n');
//    var seasonsObj = JSON.parse(response.responseText);
//    addSeasons(seasonsObj);
//
//}

//following function is temporary
var tempFunc = function (name, id, imgUrl) {

    var myCategory = $("<div>");
    var CatText = $("<div>");
    CatText.addClass('CategoryText');
    CatText.text(name);
    myCategory.addClass("Category col-md-6");
    myCategory.attr('id', "Cat"+id);
    var imgUrlWhole = 'img/' +imgUrl;
    var CatImg = $("<img>");
    CatImg.attr('src', imgUrlWhole );
    CatImg.addClass("img-responsive");
    myCategory.append(CatImg);
    myCategory.append(CatText);
    myLink = "summary/?id=" + id;

    var myHover = $("<div>");
    myHover.addClass('Hover');
    myCategory.hover(function(){ $(this).append(myHover);},function(){ $(this).children('.Hover').remove();})



    $(".MainSelection").append(myCategory);
    myCategory.wrap('<a class="CategoryWrap" href=' + myLink + '></a>');

}


var main = function () {
    console.log('test');
    $('#CzBut').click(function(){
        console.log('\language set to cs');
        document.cookie='Lang = cs';
        location.reload();
    });

    $('#EnBut').click(function(){
        console.log('\language set to en');
        document.cookie='Lang = en';
        location.reload();
    });
    
//    //request to list all root categories
//    var xhr = new XMLHttpRequest();
//    xhr.open('GET', ApiUrl + '/categories?'+lang);
//    xhr.onreadystatechange = function () {
//        if (this.readyState == 4) {
//            if (typeof cb !== "undefined") {
//                cb(this);
//            }
//            else {
//                app(this);
//            }
//        }
//    };
//    xhr.send(null);

    //Following code is only temporary
    tempFunc(getTranslation('Cat3'), 3,"animal.png");
    tempFunc(getTranslation('Cat4'), 4,"apple.png");
    tempFunc(getTranslation('Cat1'), 1,"season.png");
    tempFunc(getTranslation('Cat2'), 2,"family.png");
    
    $('.DecisionLink').text(getTranslation('DecisionLink'));
    $('.TruthDecisionLink').text(getTranslation('TruthDecisionLink'));
    $('.LoginLink').text(getTranslation('LoginLink'));
    $('.Username').text(getTranslation('Username'));
    $('.Password').text(getTranslation('Password'));
    $('.forgot').text(getTranslation('forgot'));
    $('.signIn').text(getTranslation('signIn'));
    document.title = getTranslation('HomeTitle');
}

main();
//$(document).ready(main);