var categoryNumber = "";
var wordNumber = "";

var addNewWordPopup = function(categoryId){
	categoryNumber = categoryId.split('&')[1];
	loadPopup();
	initNewWordPopup();	
};

var editWordPopup = function(id){
	console.log(id);
	wordNumber = id.split('&')[1];
	loadEditWordPopup();
	initEditWordPopup();	
};

var removeWordPopup = function(id){
	console.log(id);
	wordNumber = id.split('&')[1];
	initRemoveWordPopup();
};
function loadPopup() {
	
	$("#popupContent").empty();
	//creating inputs	
	var inputName = document.createElement("input");
	inputName.setAttribute("type", "text");
	inputName.setAttribute("id", "popName");
	var inputImg = document.createElement("input");
	inputImg.setAttribute("type", "file");
	inputImg.setAttribute("id", "popImg");
	var inputVideo = document.createElement("input");
	inputVideo.setAttribute("type", "file");
	inputVideo.setAttribute("id", "popVideo");
	var inputSound = document.createElement("input");
	inputSound.setAttribute("type", "file");
	inputSound.setAttribute("id", "popSound");
	var inputRadioVideo = document.createElement("input");
	inputRadioVideo.setAttribute("type", "radio");
	inputRadioVideo.setAttribute("name", "popSignRadio");
	inputRadioVideo.setAttribute("value", "1");
	var inputRadioLink = document.createElement("input");
	inputRadioLink.setAttribute("type", "radio");
	inputRadioLink.setAttribute("name", "popSignRadio");
	inputRadioLink.setAttribute("value", "0");
	var inputSignVideo = document.createElement("input");
	inputSignVideo.setAttribute("type", "file");
	inputSignVideo.setAttribute("id", "popSignVideo");
	var inputSignLink = document.createElement("input");
	inputSignLink.setAttribute("type", "url");
	inputSignLink.setAttribute("id", "popSignLink");
	var inputAddButton = document.createElement("input");
	inputAddButton.setAttribute("type", "submit");
	inputAddButton.setAttribute("id", "popupAddBtn");
	inputAddButton.setAttribute("value", "Přidat");	
	var inputCancelButton = document.createElement("input");
	inputCancelButton.setAttribute("type", "button");
	inputCancelButton.setAttribute("id", "popupCloseBtn");
	inputCancelButton.setAttribute("value", "Zrušit");
	//creating labels for inputs
	var labelName = document.createElement("label");
	labelName.htmlFor = "popName";
	labelName.innerHTML = "Název";	
	var labelImg = document.createElement("label");
	labelImg.htmlFor = "popImg";
	labelImg.innerHTML = "Obrázek";	
	var labelVideo = document.createElement("label");
	labelVideo.htmlFor = "popVideo";
	labelVideo.innerHTML = "Video";	
	var labelSound = document.createElement("label");
	labelSound.htmlFor = "popSound";
	labelSound.innerHTML = "Zvuk";	
	var labelSignVideo = document.createElement("label");
	labelSignVideo.htmlFor = "popSignVideo";
	labelSignVideo.innerHTML = "Znak - video";	
	var labelSignLink = document.createElement("label");
	labelSignLink.htmlFor = "popSignLink";
	labelSignLink.innerHTML = "Znak - link";	
	//adding elements to popup
	$("#popupContent").append(labelName);
	$("#popupContent").append(inputName);
	var mybr = document.createElement('br');	
	$("#popupContent").append(mybr);
	$("#popupContent").append(labelImg);
	$("#popupContent").append(inputImg);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(labelVideo);
	$("#popupContent").append(inputVideo);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(labelSound);
	$("#popupContent").append(inputSound);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputRadioVideo);
	$("#popupContent").append(labelSignVideo);
	$("#popupContent").append(inputSignVideo);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputRadioLink);
	$("#popupContent").append(labelSignLink);
	$("#popupContent").append(inputSignLink);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputAddButton);
	$("#popupContent").append(inputCancelButton);	
	//popup appearance
	$("#toPopup").fadeIn(500);
	$("#backgroundPopup").css("opacity", "0.7"); 
	$("#backgroundPopup").fadeIn(1);
}

function loadEditWordPopup() {
	
	$("#popupContent").empty();
	//creating inputs	
	var inputName = document.createElement("input");
	inputName.setAttribute("type", "text");
	inputName.setAttribute("id", "popName");
	/*
	var inputImg = document.createElement("input");
	inputImg.setAttribute("type", "file");
	inputImg.setAttribute("id", "popImg");
	var inputVideo = document.createElement("input");
	inputVideo.setAttribute("type", "file");
	inputVideo.setAttribute("id", "popVideo");
	var inputSound = document.createElement("input");
	inputSound.setAttribute("type", "file");
	inputSound.setAttribute("id", "popSound");
	var inputRadioVideo = document.createElement("input");
	inputRadioVideo.setAttribute("type", "radio");
	inputRadioVideo.setAttribute("name", "popSignRadio");
	inputRadioVideo.setAttribute("value", "1");
	var inputRadioLink = document.createElement("input");
	inputRadioLink.setAttribute("type", "radio");
	inputRadioLink.setAttribute("name", "popSignRadio");
	inputRadioLink.setAttribute("value", "0");
	var inputSignVideo = document.createElement("input");
	inputSignVideo.setAttribute("type", "file");
	inputSignVideo.setAttribute("id", "popSignVideo");
	var inputSignLink = document.createElement("input");
	inputSignLink.setAttribute("type", "url");
	inputSignLink.setAttribute("id", "popSignLink");
	*/
	var inputEditButton = document.createElement("input");
	inputEditButton.setAttribute("type", "submit");
	inputEditButton.setAttribute("id", "popupEditBtn");
	inputEditButton.setAttribute("value", "Uložit");	
	var inputCancelButton = document.createElement("input");
	inputCancelButton.setAttribute("type", "button");
	inputCancelButton.setAttribute("id", "popupCloseBtn");
	inputCancelButton.setAttribute("value", "Zrušit");
	//creating labels for inputs
	var labelName = document.createElement("label");
	labelName.htmlFor = "popName";
	labelName.innerHTML = "Název";
	/*	
	var labelImg = document.createElement("label");
	labelImg.htmlFor = "popImg";
	labelImg.innerHTML = "Obrázek";	
	var labelVideo = document.createElement("label");
	labelVideo.htmlFor = "popVideo";
	labelVideo.innerHTML = "Video";	
	var labelSound = document.createElement("label");
	labelSound.htmlFor = "popSound";
	labelSound.innerHTML = "Zvuk";	
	var labelSignVideo = document.createElement("label");
	labelSignVideo.htmlFor = "popSignVideo";
	labelSignVideo.innerHTML = "Znak - video";	
	var labelSignLink = document.createElement("label");
	labelSignLink.htmlFor = "popSignLink";
	labelSignLink.innerHTML = "Znak - link";
	*/	
	//adding elements to popup
	$("#popupContent").append(labelName);
	$("#popupContent").append(inputName);
	/*
	var mybr = document.createElement('br');	
	$("#popupContent").append(mybr);
	$("#popupContent").append(labelImg);
	$("#popupContent").append(inputImg);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(labelVideo);
	$("#popupContent").append(inputVideo);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(labelSound);
	$("#popupContent").append(inputSound);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputRadioVideo);
	$("#popupContent").append(labelSignVideo);
	$("#popupContent").append(inputSignVideo);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputRadioLink);
	$("#popupContent").append(labelSignLink);
	$("#popupContent").append(inputSignLink);
	*/
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputEditButton);
	$("#popupContent").append(inputCancelButton);	
	//popup appearance
	$("#toPopup").fadeIn(500);
	$("#backgroundPopup").css("opacity", "0.7"); 
	$("#backgroundPopup").fadeIn(1);
}

function disablePopup() {	
	$("#toPopup").fadeOut("normal");
	$("#backgroundPopup").fadeOut("normal");
}
function initNewWordPopup(){
	$(this).keyup(function(event) 
	{
		if (event.which == 27) { 
			disablePopup();
		}
	});
	
        $("#backgroundPopup").click(function() 
        {
		disablePopup();
	});
	
	$("#popupCloseBtn").click(function() 
	{
		disablePopup();
	});
	
	$("#popupAddBtn").click(function() 
	{
		if($('input[name="popSignRadio"]:checked').size() > 0)
		{
			addWord();
		}
		else
		{
			alert("Choose sign type");	
		}
	});	
}
function initEditWordPopup(){
	
	var result = "";
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', ApiUrl + "/words/"+ wordNumber +'?'+lang,false);
	xhr.send(null);	
	if(xhr.status == 200)
	{
		var word = JSON.parse(xhr.responseText);
		result = new Array(word[0].id,word[0].name,word[0].img,word[0].video,word[0].sound,word[0].sign,word[0].categoryId);
		console.log(result);
	}
	else
	{
		console.log(false)
	}
	var elem = document.getElementById("popName");
	console.log(elem);
	elem.value = result[1];
	/*
	var elem = document.getElementById("popImg");
	elem.value = result[2];
	var elem = document.getElementById("popVideo");
	elem.value = result[3];
	var elem = document.getElementById("popSound");
	elem.value = result[4];
	var elem = document.getElementById("popSign");
	elem.value = result[5];
	*/	
	
	$(this).keyup(function(event) 
	{
		if (event.which == 27) { 
			disablePopup();
		}
	});
	
        $("#backgroundPopup").click(function() 
        {
		disablePopup();
	});
	
	$("#popupCloseBtn").click(function() 
	{
		disablePopup();
	});
	
	$("#popupEditBtn").click(function() 
	{
		//if($('input[name="popSignRadio"]:checked').size() > 0)
		//{
			editWord(result);
		//}
		//else
		//{
		//	alert("Choose sign type");	
		//}
	});	
}

function initRemoveWordPopup(){
	
	$("#popupContent").empty();
	
	var message = document.createElement("p");
	message.innerHTML = "Opravdu chcete smazat toto slovo?";
	var inputAddButton = document.createElement("input");
	inputAddButton.setAttribute("type", "submit");
	inputAddButton.setAttribute("id", "popupRemoveBtn");
	inputAddButton.setAttribute("value", "Smazat");	
	var inputCancelButton = document.createElement("input");
	inputCancelButton.setAttribute("type", "button");
	inputCancelButton.setAttribute("id", "popupCloseBtn");
	inputCancelButton.setAttribute("value", "Zrušit");
	
	$("#popupContent").append(message);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	var mybr = document.createElement('br');
	$("#popupContent").append(mybr);
	$("#popupContent").append(inputAddButton);
	$("#popupContent").append(inputCancelButton);
	
	//popup appearance
	$("#toPopup").fadeIn(500);
	$("#backgroundPopup").css("opacity", "0.7"); 
	$("#backgroundPopup").fadeIn(1);
	
	$(this).keyup(function(event) 
	{
		if (event.which == 27) { 
			disablePopup();
		}
	});
	
        $("#backgroundPopup").click(function() 
        {
		disablePopup();
	});
	
	$("#popupCloseBtn").click(function() 
	{
		disablePopup();
	});
	
	$("#popupRemoveBtn").click(function() 
	{
		removeWord();
	});	
}

function addWord(){
	
	var name = $("#popName").val();
	var img = $("#popImg").val();
	var video = $("#popVideo").val();
	var sound = $("#popSound").val();
	var sign = "";
	
	if($('input[name="popSignRadio"]:checked').val() == "1")
	{
		sign = $("#popSignVideo").val();
	}
	else
	{
		sign = $("#popSignLink").val();
	}	
	
	var xhr = new XMLHttpRequest();
	xhr.open('POST', ApiUrl + '/words/1'); //bude se posílat bez parametru, API zatím odpovídá pouze na parametr
	xhr.setRequestHeader("Content-Type", "text/plain");
	xhr.onreadystatechange = function () {
	  if (this.readyState == 4) {
	    if (typeof cb !== "undefined") {
	      cb(this);
	    }
	    else 
	    {
	    	    if(this.status == 201)
	    	    {
	    	    	alert("Success");
	    	    	disablePopup();
	    	    	location.reload();
	    	    }
	    	    else
		    {
		    	alert("Failed");	    
		    }
	      
	    }
	  }
	};
	xhr.send("{ \"lang\": \""+lang+"\", \"name\": \""+name+"\", \"img\": \""+img+"\", \"video\": \""+video+"\", \"sound\": \""+sound+"\", \"sign\": \""+sign+"\", \"categoryId\": "+categoryNumber+" }");
	console.log(lang);
	console.log(name);
	console.log(img);
	console.log(video);
	console.log(sound);
	console.log(sign);
	console.log(categoryNumber);
};

function editWord(result){
	
	var name = $("#popName").val();
	//var img = $("#popImg").val();
	//var video = $("#popVideo").val();
	//var sound = $("#popSound").val();
	//var sign = "";
	
	//if($('input[name="popSignRadio"]:checked').val() == "1")
	//{
	//	sign = $("#popSignVideo").val();
	//}
	//else
	//{
	//	sign = $("#popSignLink").val();
	//}	
	var wordId = result[0];
	var img = result[2];
	var video = result[3];
	var sound = result[4];
	var sign = result[5];
	var categoryId = result[6];
	
	
	var xhr = new XMLHttpRequest();
	xhr.open('POST', ApiUrl + '/words/'+wordId); 
	xhr.setRequestHeader("Content-Type", "text/plain");
	xhr.onreadystatechange = function () {
	  if (this.readyState == 4) {
	    if (typeof cb !== "undefined") {
	      cb(this);
	    }
	    else 
	    {
	    	    if(this.status == 201)
	    	    {
	    	    	alert("Success");
	    	    	disablePopup();
	    	    	location.reload();
	    	    }
	    	    else
		    {
		    	alert("Failed");	    
		    }
	      
	    }
	  }
	};
	xhr.send("{ \"lang\": \""+lang+"\", \"name\": \""+name+"\", \"img\": \""+img+"\", \"video\": \""+video+"\", \"sound\": \""+sound+"\", \"sign\": \""+sign+"\", \"categoryId\": "+categoryId+" }");
	console.log(wordId);
	console.log(lang);
	console.log(name);
	console.log(img);
	console.log(video);
	console.log(sound);
	console.log(sign);
	console.log(categoryNumber);
};

function removeWord(){
	
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', ApiUrl + '/words/' + wordNumber);
	xhr.setRequestHeader("Content-Type", "text/plain");
	xhr.onreadystatechange = function () {
	  if (this.readyState == 4) {
	    if (typeof cb !== "undefined") {
	      cb(this);
	    }
	    else 
	    {
	    	    if(this.status == 204)
	    	    {
	    	    	alert("Success");
	    	    	disablePopup();
	    	    	location.reload();
	    	    }
	    	    else
		    {
		    	alert("Failed");	    
		    }
	    }
	  }
	};
	xhr.send(null);
}

var makeAccordColaps = function () {
    $(".MainSelectionList").accordion({
        collapsible:true
    });
};

var listWordsInCategory = function (numOfCat) {
    var xhr = new XMLHttpRequest();
	//change for backend ... TODO: language
    xhr.open('GET', ApiUrl + '/categorywords/' + numOfCat, false);
    xhr.send(null);
    var words = JSON.parse(xhr.responseText);
    console.log(words[0].img);
    return words;
};


var fillCategories = function (categories) {

    //for cycle through categories
    for (var i = 0; i < categories.length; i++) {
        var title = $("<h3>");
        title.text(categories[i].name)
        $(".MainSelectionList").append(title);
        var content = $("<div>");
        content.addClass("AdministrationWordsList");
        var words = listWordsInCategory(categories[i].id)

        //for cycle adding all words in category
        for (var j = 0; j < words.length; j++) {
            var myWord = $("<div>");
            var wordImg = '..' + words[j].img;
            var CatImg = $("<img>");
            CatImg.attr('src', wordImg);
            myWord.append(CatImg);
            myWord.css("position", "relative");
            myWord.addClass("AdministrationWords");
            var options = $("<div>");
            options.addClass('AdministrationOptions');
            var Edit = $("<div>");
            Edit.addClass('Edit');
            //Edit.attr("id", "edit"+"&"+words[j].id+"&"+categories[i].id);
            options.append(Edit);
            var Remove = $("<div>");
            Remove.addClass('Remove');
            //Remove.attr("id", "remove"+"&"+words[j].id+"&"+categories[i].id);
            options.append(Remove);
            myWord.hover(function () {
                $(this).append(options);
                  Edit.click(function(){editWordPopup(this.parentNode.parentNode.id);});
                Remove.click(function(){removeWordPopup(this.parentNode.parentNode.id);});
            }, function () {
                $(this).children('.AdministrationOptions').remove();

            });
            myWord.attr("id", "word"+"&"+words[j].id);
            $(content).append(myWord);


        }
        $(".MainSelectionList").append(content);
        console.log(categories[i].name);

        //adding button
	var addButton = $("<div>");


        var CatImg = $("<img>");
        CatImg.attr('src', '../img/addButton.png' );
        addButton.append(CatImg);
        addButton.addClass("AdministrationWords");        
        addButton.addClass("AddButton");
        addButton.attr("id","addBtn"+"&"+categories[i].id);
        $(content).append(addButton);
        addButton.click(function(){addNewWordPopup(this.id);});        
    };

    makeAccordColaps();

};

var main = function () {
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', ApiUrl + '/categories?' + lang);
	xhr.onreadystatechange = function () {
	if (this.readyState == 4) {
	    if (typeof cb !== "undefined") {
		cb(this);
	    }
	    else {
		var categories = JSON.parse(this.responseText);
		fillCategories(categories);
	    }
	}
	};
	xhr.send(null);


};

main();